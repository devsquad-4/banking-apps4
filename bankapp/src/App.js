import logo from './logo.svg';
import './App.css';
import SignInSide from './bankcomponents/LoginUI';
import Forget from './bankcomponents/Forget';
import Email from './bankcomponents/Email';
import PasswordReset from './bankcomponents/PasswordReset';
import React from "react";
import './Thestyle.css';
import DisplayJSON from './bankcomponents/SetJSON';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  
  return (
      <div>
        <Router>
        <Switch>
          <Route path="/resetpassword">
           <PasswordReset/>
          </Route>
          <Route path="/forgetpassword">
            <Forget />
          </Route>
          <Route path="/emailsent">
           <Email/>
          </Route>
          <Route path="/">
           <SignInSide/>
           <DisplayJSON></DisplayJSON>
          </Route>
         
        </Switch>
        </Router>
      </div>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

export default App;
