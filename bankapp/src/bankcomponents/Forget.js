import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CreateIcon from '@material-ui/icons/Create';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {useEffect, useState } from "react";
import emailjs from 'emailjs-com';


function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://material-ui.com/">
          Your Website
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }




  const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
      width: '100',
      backgroundColor: 'orange',
    },
    image: {
      backgroundImage: 'url(https://blog-www.pods.com/wp-content/uploads/2019/04/MG_1_1_New_York_City-1.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      
      
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
     
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));



export default function Forget() {
    const classes = useStyles();
    
    function sendEmail(e) {
      e.preventDefault();
      //console.log("HEllo");
  
      emailjs.sendForm('service_0vws2zk', 'template_5g2ydxj', e.target, 'user_zUY9QbzZYe2yqzV8JB7zE')
        
        .then((result) => {
            console.log(result.text);
            window.setTimeout(function() {
              window.location.href = 'http://localhost:3000/emailsent';
          }, 500);
            
        }, (error) => {
            console.log(error.text);
        });

        
    }



    return (
      <Grid container component="main" className={classes.root} >
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square style={{backgroundColor: "Orange"}}>
       <div className={classes.paper}>
          <h2>Optimum DigiBank</h2>
          
          
  
          <form className={classes.form} noValidate id="theform" onSubmit={sendEmail}>

              <input type="hidden" name="subject" value="Reset Password"/>
              <input type="hidden" name="name" value="Bank User"/>
              <input type="hidden" name="admin" value="Optimum Admin"/>
              <input type="hidden" name="link" value="http://localhost:3000/resetpassword" />
              <input name="message" type="hidden" value="Click on the link to reset your password"/>

              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                //href="/emailsent"
                className={classes.submit}
              >
                Submit
              </Button>
          
              <Box mt={5}>
                <Copyright />
              </Box>
          </form>
  
  
          </div>
        </Grid>
      </Grid>
    );
  }


  